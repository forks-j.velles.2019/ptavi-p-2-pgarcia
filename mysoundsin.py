"Ejercicio 5"

import math
from mysound import Sound


class SoundSin(Sound):
    def __init__(self, duration, frequency, amplitude):
        """
        Initializes a new instance of the class with a sine wave.

        :param duration: The duration of the sound in seconds
        :param frequency: The frequency of the sine wave in Hz
        :param amplitude: The amplitude of the sine wave
        """
        # Initialize the base Sound class with the given duration
        super().__init__(duration)

        # Generate the sine wave in the buffer
        self.sin(frequency, amplitude)
